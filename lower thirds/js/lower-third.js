
var bc = new BroadcastChannel('obs-lower-thirds-channel');
var displayItem=function(item){
	bc.postMessage("display");
    var lowerThirdList = $('#lower-third-list');
	$(lowerThirdList).children().each(function(){
		hideItem(this);
	});
	$(item).addClass('active');
	console.log('displayItem');
}
var hideItem=function(item){
	bc.postMessage("hide");
	$(item).removeClass('active');
	console.log('hideItem');
}
$(document).ready(function(){
	var data=JSON.parse($('#config-lower-third').html())
	if(data.type == 'lower-third-list'){
		$.each(data.elements,function(index,element){
			var lowerThirdList = $('#lower-third-list');
			if(lowerThirdList != undefined){
				lowerThirdListPattern = lowerThirdList.find('.pattern');
				lowerThirdItem=lowerThirdListPattern.clone();
				$.each(element,function(key,value){
					lowerThirdItem.find('.'+key).text(value);
				});
				lowerThirdItem.removeClass('pattern');
				lowerThirdItem.on('click',function(){
					if($(this).hasClass('active')){
						hideItem(this);
					}else{
						displayItem(this);
					}
				});
				lowerThirdList.append(lowerThirdItem);
			}
		});
	};
});